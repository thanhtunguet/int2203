#include <iostream>
#include <fstream>

using namespace std;

typedef struct Node ListNode;

/**
 * Struct Node
 * Other name: ListNode
 */

struct Node {
    int data;
    Node *next;
    
    Node() {
        next = NULL;
    }
    
    Node(int data) {
        this->data = data;
        this->next = NULL;
    }
    
    Node(int data, Node *next) {
    	this->data = data;
    	this->next = next;
	}
};

/**
 * Struct LinkedList
 */

struct LinkedList {
    Node *first;
    
    LinkedList() {
        first = NULL;
    }
    
    void insert(int data) {
        if (first == NULL) {
        	first = new Node(data);
		} else {
			Node *node = first;
			while (node->next != NULL) {
				node = node->next;
			}
			node->next = new Node(data);
		}
    }
    
    void addFirst(int data) {
    	Node *node = new Node(data);
    	node->next = first;
    	first = node;
	}
	
	void removeFirst() {
		if (first != NULL) {
			Node *temp = first;
			first = first->next;
			delete temp;
		}
	}
};

/**
 * This function create a new list from a file contains a list of numbers
 * @param fileName path of the file contains numbers
 * @return LinkedList
 */

LinkedList *newList(char *fileName) {
    fstream fp;
    fp.open(fileName, ios::in);
    int n;
    LinkedList *lst = new LinkedList();
    while (fp >> n) {
    	lst->insert(n);
	}
    fp.close();
    return lst;
}

/**
 *	This function create a new node
 * @param v data of the node would be created
 * @return Node*
 */

ListNode *newListNode(int v) {
	return new Node(v);
}

/**
 * This function print a list
 * @param l a list
 * @return void
 */

void printList(LinkedList *l) {
	Node *node = l->first;
	int i = 0;
	while (node != NULL) {
		if (i) {
			cout << " ";
		}
		i++;
		cout << node->data;
		node = node->next;
	}
}

/**
 * This function add a node in the first of the list
 * @param l a LinkedList
 * @param v data of the node which would be added
 * @return void
 */

void addFirst(LinkedList *l, int v) {
	l->addFirst(v);
}

/**
 * This function add a node after the last node of list
 * @param l a LinkedList
 * @param v data of the node which would be added
 * @return void
 */

void addLast(LinkedList *l, int v) {
	l->insert(v);
}

/**
 * This function delete the first node if exists
 * @return void
 */

void removeFirst(LinkedList *l) {
	l->removeFirst();
}

/**
 * This function find a node contains a value considered
 * @param l a LinkedList
 * @param v an integer to find
 * @return int index of the node found, otherwise -1
 */
 
int findIndex(LinkedList *l, int v) {
	Node *node = l->first;
	int i = 0; // Index of node
	while (node != NULL) {
		if (node->data == v) {
			return i;
		}
		i++;
		node = node->next;
	}
	return -1;
}

// Main function

int main(int argc, char *argv[]) {

	LinkedList *lst = newList("Bai1.txt");
	lst->insert(5);
	lst->addFirst(199);
	addLast(lst, 1000);
	removeFirst(lst);
	printList(lst);
	cout << endl << findIndex(lst, 1000);
	return 0;
}

