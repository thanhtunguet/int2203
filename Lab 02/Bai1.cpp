#include <iostream>
#include <fstream>

using namespace std;

static const int MAX = 500;

int *newArray(const char *fileName, int &length) {
	length = 0;
	fstream fp;
	fp.open(fileName, ios::in);
	int N; // temporary int stores data read from file
	while (fp >> N) {
		if (length < MAX)
			length++;
		else
			break;
	}
	fp.clear();
	fp.seekg(0, ios::beg);
	int i = 0;
	int *array = new int[length];
	while (fp >> N && i < length) {
		array[i] = N;
		i++;
	}
	fp.close();
	return array;
}

void printArray(const int *array, const int &length) {
	for (int i = 0; i < length; i++) {
		if (i) cout << " ";
		cout << array[i];
	}
	cout << endl;
}

int *insertNum(int *array, int &length, const int value, const int index) {
	if (index <= 0 || index >= length) {
		return array;
	} else {
		length++;
		int *myNewArray = new int[length];
		for (int i = 0; i < index; i++) {
			myNewArray[i] = array[i];
		}
		myNewArray[index] = value;
		for (int i = index+1; i < length; i++) {
			myNewArray[i] = array[i-1];
		}
		return myNewArray;
	}
}

int *removeIndex(int *array, int &length, const int index) {
	if (length <= 0) {
		return array;
	} else {
		if (index <= 0 && index >= length) {
			return array;
		} else {
			length--;
			int *myNewArray = new int[length];
			for (int i = 0; i < index; i++) {
				myNewArray[i] = array[i];
			}
			for (int i = index; i < length; i++) {
				myNewArray[i] = array[i+1];
			}
			return myNewArray;
		}
	}
	
}

int findIndex(const int *array, const int &length, const int value) {
	for (int i = 0; i < length; i++) {
		if (array[i] == value) {
			return i;
		}
	}
	return -1;
}

int main() {
	int length;
	int* array = newArray("Bai1.txt", length);
	printArray(array, length);
	cout << findIndex(array, length, 4) << endl;
	int *arrayAfterInsert = insertNum(array, length, 13, 4);
	printArray(arrayAfterInsert, length);
	int *arrayAfterRemove = removeIndex(arrayAfterInsert, length, 6);
	printArray(arrayAfterRemove, length);
}
