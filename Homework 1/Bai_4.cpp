#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int gCD(int a, int b) {
	
    // Greatest common divisor of a and b
    
    int c;
    
    while (a != b) {
    	
        c = abs(a - b);
        a = min(a, b);
        b = c;
    }
    
    return b;
}

int main(int argc, char *argv[]) {
	
    fstream f_inp, f_out;
    
    f_inp.open("gCD_input.txt", ios::in);   // Input file
    f_out.open("gCD_output.txt", ios::out); // Output file
    
    int a, b;
    
    while (! f_inp.eof()) {
    	
        f_inp >> a >> b;
        // each line has two integers: a and b
        f_out << gCD(a, b) << endl;
        // the gCD of a and b
    }
    
    f_inp.close();
    f_out.close();
    
	return 0;
}
