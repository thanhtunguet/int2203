#include <iostream>
#include <fstream>

using namespace std;

struct Point {
	
	double x, y;
	
	Point(double* x = NULL, double* y = NULL) {
		
		if (x != NULL) {
			this->x = *x;
		}
		
		if (y != NULL) {
			this->y = *y;
		}
	}
	
	void getValues(fstream *fp) {
		
		*fp >> this->x >> this->y;
	}
};

struct LineEquation {
	
	double a, b, c;
	
	LineEquation(Point A, Point B) {
		
		double nx = A.x - B.x;
		double ny = A.y - B.y;
		
		this->a = ny;
		this->b = -nx;
		
		this->c = - (a * A.x + b * A.y);
	}
};

struct Line {
	
	Point beg, end;
	LineEquation *e;
	
	Line(Point *A, Point *B) {
		
			this->beg = *A;
			this->end = *B;
			this->e = new LineEquation(this->beg, this->end);
	}
	
	bool sameSide(Line *d) {
		
		double A = this->e->a * d->beg.x + this->e->b * d->beg.y + this->e->c;
		double B = this->e->a * d->end.x + this->e->b * d->end.y + this->e->c;
		return (A * B > 0);
	}
	
	~Line() {
		
		delete this->e;
	}
};

int main(int argc, char* argv[]) {
	
	fstream fp;
	fp.open("points.txt", ios::in);
	
	Point A, B, C, D;
	
	A.getValues(&fp);
	B.getValues(&fp);
	C.getValues(&fp);
	D.getValues(&fp);
	
	fp.close();
	
	Line *AB = new Line(&A, &B);
	Line *CD = new Line(&C, &D);
	
	if ( (!AB->sameSide(CD)) && (!CD->sameSide(AB)) ) {
		cout << "YES";
	} else {
		cout << "NO";
	}
	
	delete AB, CD;

	return 0;
}
