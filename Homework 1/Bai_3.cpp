#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int main(int argc, char* argv[]) {
	
	double a, b;
	double R = 0, I = 0;
	fstream fp;
	fp.open("complex.txt", ios::in);
	while (! fp.eof()) {
		fp >> a >> b;
		R += a;
		I += b;
	}
	fp.close();
	cout << R;
	if (I < 0) {
		cout << " - " << -I;
	} else {
		cout << " + " << I;
	}
	cout << "i";
	return 0;
}
