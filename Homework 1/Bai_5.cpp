#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

double *addNewElement(double d, double *p, int size) {
	
	double *q = new double[size+1];
	
	for (int i = 0; i < size; i++) {
		
		q[i] = p[i];
	}
	
	q[size] = d;
	delete [] p;
	return q;
}

int main(int argc, char *argv[]) {
    
    fstream fp;
    fp.open("numbers.txt", ios::in);
    
    double *p = new double[0];
    double d;
    int len = 0;
    
    while (! fp.eof()) {
    	
    	fp >> d;
    	p = addNewElement(d, p, len);
    	len++;
	}
	
	fp.close();
	fp.open("numbers.txt", ios::out);
	sort(p, p + len);
		
	for (int i = 0; i < len; i++) {
		if (i > 0) {
			fp << " ";
		}
		fp << p[i];
	}
    fp.close();
    delete [] p;
	return 0;
}

