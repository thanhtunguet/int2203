#include <iostream>
#include <fstream>

using namespace std;
int main(int argc, char *argv[]) {
    string str;
    cout << "Input string: ";
    getline(cin, str);
    fstream fp;
    fp.open("rev.txt", ios::out);
    for (int i = str.length() - 1; i >= 0; i--) {
        fp << str[i];
    }
    fp.close();
	return 0;
}

