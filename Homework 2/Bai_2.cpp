#include <iostream>

using namespace std;

long long pow_base_2(int n) {
    long long result = 1;
    if (n > 0) {
        for (int i = 0; i < n; i++) {
            result *= 2;
        }
    }
    return result;
}

int main(int argc, char *argv[]) {
    int n;
    cin >> n;
    if (n < 0) {
        cout << "! N >= 0";
    } else {
        if (n >= 63) {
            cout << "! n < 63";
        } else {
            cout << "2 ^ " << n << " = " << pow_base_2(n);
        }
    }
	return 0;
}

