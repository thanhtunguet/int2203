#include <iostream>

using namespace std;

class MinAndMax {
    
    int Min;
    int Max;
    int num_of_comp;
    
public:
    void find(int *nums, int N) {
        this->num_of_comp = 0;
        this->Max = nums[0];
        this->Min = nums[0];
        
        int MaxT;
        int MinT;
        
        N--;
        
        for (int i = 1; i < N; i += 2) {
            
            if (nums[i] > nums[i+1]) { // 1 comparison
                MaxT = nums[i];
                MinT = nums[i+1];
            } else {
                MaxT = nums[i+1];
                MinT = nums[i];
            }
            if (MaxT > this->Max) { // 1 comparison
                this->Max = MaxT;
            }
            if (MinT < this->Min) { // 1 comparison
                this->Min = MinT;
            }
            
            this->num_of_comp += 3;
        }
        if (N % 2 == 1) { // Neu con so cuoi cung chua duoc so sanh
            if (this->Min > nums[N]) {
                this->Min = nums[N];
            } else {
                if (this->Max < nums[N]) {
                    this->Max = nums[N];
                }
                this->num_of_comp++;
            }
            this->num_of_comp++;
        }
        this->num_of_comp++;
    }
    
    int getMax() {
        return this->Max;
    }
    
    int getMin() {
        return this->Min;
    }
    
    int getNumOfComp() {
        return this->num_of_comp;
    }
};

int main(int argc, char *argv[]) {
    
    int N;
    cout << "Enter N: ";
    cin >> N;
    int *nums = new int[N];
    for (int i = 0; i < N; i++) {
        cin >> nums[i];
    }
    MinAndMax *m = new MinAndMax;
    m->find(nums, N);
    cout << "Min = " << m->getMin() << endl;
    cout << "Max = " << m->getMax() << endl;
    cout << "Num of comparisons = " << m->getNumOfComp();
    delete [] nums;
    delete m;
	return 0;
}

