#include <iostream>

using namespace std;

class RowsRich1 {
    int **matrix;
    int N;
    int Row;
    
    public:
            
    RowsRich1(int n) {
        this->N = n;
        this->matrix = new int*[N];
        for (int i = 0; i < N; i++) {
            matrix[i] = new int[N];
            for (int j = 0; j < N; j++) {
                cin >> matrix[i][j];
            }
        }
    }
    
    ~RowsRich1() {
        for (int i = 0; i < N; i++) {
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    
        
    int findRow() {
        int num_i = 0;
        int num_i_tmp = 0;
        int row = -1;
        for (int i = 0; i < this->N; i++) {
            for (int j = this->N - 1; j >= 0; j--) {
                if (this->matrix[i][j] == 1) {
                    num_i_tmp = j+1;
                    break;
                }
            }
            if (num_i_tmp == this->N) {
                return i;
            } else if (num_i_tmp > num_i) {
                num_i = num_i_tmp;
                row = i;
            }
        }
        return row;
    }
};

int main(int argc, char *argv[]) {
    int N;
    cin >> N;
    RowsRich1 *r = new RowsRich1(N);
    cout << r->findRow(); // Tim ra hang chua nhieu so 1 nhat
    delete r;
	return 0;
}

