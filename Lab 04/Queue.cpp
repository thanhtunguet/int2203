#include <iostream>
#include <fstream>
using namespace std;

struct Node {
	int data;
	Node *next;
	
	/**
	 * Constructors
	 */
	
	Node(int data) {
		this->data = data;
		this->next = NULL;
	}
	
	Node(int data, Node *next) {
		this->data = data;
		this->next = next;
	}
	
	/**
	 * Setters and getters
	 */
	
	void setNext(Node *next) {
		this->next = next;
	}
	
	Node *getNext() {
		return this->next;
	}
	
	int getData() {
		return this->data;
	}
};

struct Queue {
	
	/**
	 * The first node of the queue
	 */ 
	Node *head;
	
	/**
	 * Queue constructor
	 */ 
	
	Queue() {
		head = NULL;
	}
	
	/**
	 * Queue destructor
	 */
	
	~Queue() {
		Node *tmp = head;
		while (! isEmpty()) {
			head = head->getNext();
			delete tmp;
			tmp = head;
		}
	}
	
	/**
	 * @return true if empty, otherwise false
	 * */
	
	bool isEmpty() {
		return (head == NULL);
	}
	
	/**
	 * print this queue
	 */ 
	
	void printQueue(bool newLine = false) {
		Node *tmp = head;
		if (! isEmpty()) {
			int i = 0;
			while (! isEmpty()) {
				head = head->getNext();
				if (i) {
					cout << " ";
				}
				cout << tmp->getData();
				tmp = head;
				i++;
			}
			if (newLine) {
				cout << endl;
			}
		}
	}
	
	/**
	 * Add a node to the queue
	 * */
	
	Node *enQueue(int data) {
		Node *node = new Node(data, NULL);
		Node *tmp = head;
		if (tmp == NULL) {
			head = node;
		} else {
			while (tmp->getNext() != NULL) {
				tmp = tmp->getNext();
			}
			tmp->setNext(node);
		}
		return node;
	}
	
	/**
	 * Remove a node from the queue
	 * @return int data of the node removed
	 * */
	
	int deQueue() {
		if (head == NULL) {
			return -1;
		}
		Node *tmp = head;
		head = head->getNext();
		int data = tmp->getData();
		delete tmp;
		return data;
	}
	
	/**
	 * Get data of the first node
	 * @return int
	 * */
	
	int getHead() {
		if (head == NULL) {
			return -1;
		}
		return head->getData();
	}
};

/**
 * Create a new queue from a file
 * @return Queue*
 * */

Queue *newQueue(const char* fileName) {
	fstream fp;
	fp.open(fileName, ios::in);
	Queue *queue = new Queue();
	int N;
	while (fp >> N) {
		queue->enQueue(N);
	}
	fp.close();
	return queue;
}

int main(int argc, char *argv[]) {
	Queue *queue = newQueue("queue.txt");
	queue->enQueue(15);
	int N = queue->deQueue();
	queue->printQueue();
	cout << endl << N << endl;
}
