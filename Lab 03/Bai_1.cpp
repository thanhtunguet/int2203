#include <iostream>
#include <fstream>

using namespace std;

struct Node {
	int  data;
	Node *next;
	
	Node(const int data, Node *node = NULL) {
		this->data = data;
		this->next = node;
	}
	
	Node() {
		this->next = NULL;
	}
};

struct Stack {
	Node *top;
	int size;
	
	Stack() {
		size = 0;
		top = NULL;
	}
	
	void push(const int data) {
		Node *node = new Node(data);
		if (top == NULL) {
			top = node;
		} else {
			node->next = top;
			top = node;
		}
		size++;
	}
	
	int pop() {
		try {
			size--;
			Node *node = top;
			top = top->next;
			int data = node->data;
			delete node;
			return data;
		} catch (int e) {
			cout << "Stack is empty, cannot pop!";
		}
	}
	
	int getTop() {
		try {
			return top->data;
		} catch (int e) {
			cout << "Stack is empty, cannot getTop!";
		}
	}
};

Stack *newStack(const char* fileName) {
	fstream fp;
	fp.open(fileName, ios::in);
	int n;
	Stack *s = new Stack();
	while (fp >> n) {
		s->push(n);
	}
	fp.close();
	return s;
}

void push(Stack *s, const int data) {
	s->push(data);
}

int pop(Stack *s) {
	return s->pop();
}

bool isEmpty(const Stack *s) {
	return (s->size == 0);
}

int getTop(Stack *s) {
	return s->getTop();
}

void printStack(const Stack *s) {
	Node *node = s->top;
	int i = 0;
	while (node != NULL) {
		if (i) {
			cout << " ";
		}
		cout << node->data;
		node = node->next;
		i++;
	}
}

int main(int argc, char * argv[]) {
	Stack *s = newStack("test.txt");
	s->push(15);
	printStack(s);
	s->pop();
	s->push(20);
	cout << endl;
	printStack(s);
	return 0;
}
