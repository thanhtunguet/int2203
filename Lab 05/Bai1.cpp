#include <iostream>

using namespace std;

int findMax(int *array, const int &length) {
	int myLength = length;
	if (myLength == 1) {
		return array[0];
	}
	if (myLength == 2) {
		return (array[0] > array[1]) ? array[0] : array[1];
	}
	myLength--;
	int recursive = findMax(array, myLength);
	return (recursive > array[myLength]) ? recursive : array[myLength];
}

int main(int argc, char *argv[]) {
	int length = 10;
	int array[] = {5, 1, 6, 7, 9, 10, 15, 3, 82, 10};
	cout << findMax(array, length);
}
