#include <iostream>
#include <fstream>

using namespace std;

int maxElementPos(int *array, int length) {
	int maxE = array[0];
	int trace = 0;
	for (int i = 1; i < length; i++) {
		if (array[i] >= maxE) {
			maxE = array[i];
			trace = i;
		}
	}
	return trace;
}

void subArrayLongest(int *arrayList, const int length, int *maxArray, int index = -2) {
	if (index == -2) {
		index = length-1;
		maxArray[index] = 1;
		index--;
	}
	if (index < 0) {
		return;
	}
	for (int i = index+1; i < length; i++) {
		if (arrayList[index] <= arrayList[i]) {
			if (maxArray[index] <= maxArray[i]) {
				maxArray[index] = maxArray[i] + 1;
			}
		}
	}
	subArrayLongest(arrayList, length, maxArray, index-1);
}

int main() {
	int *arrayList;
	int parser;
	int length = 0;
	fstream fp;
	fp.open("Bai3.txt", ios::in);
	while (fp >> parser) {
		length++;
	}
	arrayList = new int[length];
	fp.clear();
	fp.seekg(0, ios::beg);
	for (int i = 0; i < length; i++) {
		fp >> parser;
		arrayList[i] = parser;	
	}
	fp.close();
	int *maxArray = new int[length];
	for (int i = 0; i < length; i++) {
		maxArray[i] = 1;
	}
	subArrayLongest(arrayList, length, maxArray);
	for (int i = 0; i < length; i++) {
		if (i > 0) cout << "\t";
		cout << arrayList[i];
	}
	cout << endl;
	for (int i = 0; i < length; i++) {
		if (i > 0) cout << "\t";
		cout << maxArray[i];
	}
	cout << endl;
	int maxPos = maxElementPos(maxArray, length);
	int maxE = arrayList[maxPos];
	for (int i = maxPos; i < length; i++) {
		if (maxE <= arrayList[i]) {
			maxE = arrayList[i];
			cout << maxE << "\t";
		}
	}
	cout << endl;
	return 0;
}