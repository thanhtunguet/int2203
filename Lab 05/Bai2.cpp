#include <iostream>

using namespace std;

void reverseI2J(int *array, const int &length, int i, int j) {
	if (0 <= i && i < j && j < length) {
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
		if (j > i + 2) {
			reverseI2J(array, length, i+1, j-1);
		}
	}
}

int main() {
	int length = 10;
	int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
	
	reverseI2J(array, length, 5, 9);
	
	for (int i = 0; i < length; i++) {
		if (i) cout << " ";
		cout << array[i];
	}
}
