#include <iostream>
#include <fstream>

using namespace std;

const int MAX_LENGTH = 100;

class Heap {
	
	int* data;
	int size;

	void swap(const int a, const int b) {
		data[a] = data[a] + data[b];
		data[b] = data[a] - data[b];
		data[a] = data[a] - data[b];
	}

	void replaceParent(const int index) {
		if (! isLeaf(index)) {
			if (hasRight(index)) {
				if (data[left(index)] < data[right(index)]) {
					swap(left(index), index);
					swap(right(index), left(index));
				} else {
					swap(right(index), index);
				}
			} else {
				swap(index, left(index));
			}
		}
	}

public:

	Heap() {
		data = new int[MAX_LENGTH + 1];
		size = 0;
	}

	~Heap() {
		delete [] data;
	}

	int get(const int index) {
		return data[index];
	}

	int length() {
		return size;
	}

	bool insert(const int value) {
		size++;
		data[size] = value;
		int currentIndex = size;
		while (currentIndex > 1 && data[currentIndex] < data[parent(currentIndex)]) {
			this->swap(currentIndex, parent(currentIndex));
			currentIndex = parent(currentIndex);
			if (currentIndex == 1) {
				break;
			}
		}
	}

	int removeMin() {
		int removingValue = data[1];
		size--;
		data[1] = data[size];
		replaceParent(1);
	}

	int left(const int index) {
		return index * 2;
	}

	int right(const int index) {
		return index * 2 + 1;
	}

	bool hasRight(const int index) {
		return ((! isLeaf(index)) && (right(index) <= size));
	}

	int parent(const int index) {
		return index / 2;
	}

	bool isLeaf(const int index) {
		return (index >= size / 2 && index <= size);
	}

	void print() {
		for(int i = 1; i <= size; i++) {
			if (i > 1) {
				cout << "\t";
			}
			cout << data[i];
		}
		cout << endl;
	}
};

Heap* priQueue(const char* filename) {
	fstream fp;
	fp.open(filename, ios::in);
	int parser;
	Heap *heap = new Heap();
	while (fp >> parser) {
		heap->insert(parser);
	}
	fp.close();
	return heap;
}

int main() {
	Heap *heap = priQueue("Heap.txt");
	heap->print();
	delete heap;
}