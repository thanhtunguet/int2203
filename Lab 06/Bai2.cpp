#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

template <typename T> struct Node { 
    T data;
    Node *left;
    Node *right;
    
    Node(T data) {
    	this->data = data;
    	left = NULL;
    	right = NULL;
    }

    Node(T data, Node *left, Node *right) {
    	this->data = data;
    	this->left = left;
    	this->right = right;
    }

    void setLeft(Node<T> left) {
    	this->left = left;
    }

    void setRight(Node<T> right) {
    	this->right = right;
    }

    bool isLeaf() {
    	return (left == NULL && right == NULL);
    }
};


template <typename T> struct BinaryTreeADT {
    Node<T> *root = NULL;

    BinaryTreeADT(Node<T> *node) {
    	root = node;
    }
};

template <typename T> bool hasLeft(BinaryTreeADT<T> tree) {
	if (tree->root == NULL) {
		return false;
	} else {
		if (tree->root->left == NULL) {
			return false;
		} else {
			return true;
		}
	}
}

template <typename T> bool hasRight(BinaryTreeADT<T> tree) {
	if (tree->root == NULL) {
		return false;
	} else {
		if (tree->root->right == NULL) {
			return false;
		} else {
			return true;
		}
	}
}

typedef BinaryTreeADT<int> *BinaryTree;

Node<int>* newNullTree(int level = 1) {
	if (level < 1) {
		return NULL;
	}
	if (level == 1) {
		return new Node<int>(0);
	}
	Node<int> *left = newNullTree(level-1);
	Node<int> *right = newNullTree(level-1);
	return new Node<int>(0, left, right);
}

void parseTreeInOrder(fstream &fp, Node<int> *root) {
	int data;
	if (root->isLeaf()) {
		fp >> data;
		root->data = data;
		return;
	}
	parseTreeInOrder(fp, root->left);
	fp >> data;
	root->data = data;
	parseTreeInOrder(fp, root->right);
}

BinaryTree createTree(const char*filename) {
	fstream fp;
	int num = 0;
	int data;
	fp.open(filename, ios::in);
	while (fp >> data) {
		num++;
	}
	
	fp.clear();
	fp.seekg(0, ios::beg);

	int height = (int) log2((double) num+1);
	Node<int> *root = newNullTree(height);
	parseTreeInOrder(fp, root);
	fp.close();
	return new BinaryTreeADT<int>(root);
}

BinaryTree left(BinaryTree tree) {
	if (tree->root == NULL) {
		return NULL;
	}
	return new BinaryTreeADT<int>(tree->root->left);
}

BinaryTree right(BinaryTree tree) {
	if (tree->root == NULL) {
		return NULL;
	}
	return new BinaryTreeADT<int>(tree->root->right);
}

void traversalLevel(BinaryTree &tree) {
	cout << tree->root->data << endl;
	if (tree->root->isLeaf()) {
		return;
	}
	BinaryTree left = new BinaryTreeADT<int>(tree->root->left);
	BinaryTree right = new BinaryTreeADT<int>(tree->root->right);
	traversalLevel(left);
	delete left;
	traversalLevel(right);
	delete right;
}

int main() {
	BinaryTree tree = createTree("Bai2.txt");
	traversalLevel(tree);
	return 0;
}