#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

template <typename T> struct Node { 
    T data;
    Node *firstChild;
    Node *nextSibling;
    
    Node(T data) {
    	this->data = data;
    }

    void setChild(Node *firstChild) {
		this->firstChild = firstChild;
	}
	
	void setNextSibling(Node *next) {
		this->nextSibling = next;
	}

	void setNextSibling(T data) {
		this->nextSibling = new Node(data);
	}
};


template <typename T> struct TreeADT {
    Node<T> *root = NULL;
};

typedef TreeADT<int> *Tree;

template <typename T> struct LinkedList {

	Node<T> *head;

	int size;

	LinkedList() {
		size = 0;
		head = NULL;
	}

	LinkedList(Node<T> *node) {
		if (node != NULL) {
			head = node;
			size++;
		}
	}

	LinkedList(string str) {
		istringstream iss(str);
		T parseData;
		if (iss >> parseData) {
			head = new Node<int>(parseData);
			size++;
		}
		Node<T> *run = head;
		while (iss >> parseData) {
			run->setNextSibling(parseData);
			size++;
			run = run->nextSibling;
		}
	}

	void print() {
		Node<T> *run = head;
		int i = 0;
		while (run != NULL) {
			if (i > 0) {
				cout << " ";
			}
			cout << run->data;
			i++;
		}
		if (i == 0) {
			cout << "NULL";
		}
		cout << endl;
	}
};

template <typename T> void readNode(fstream &fp, LinkedList<T> *list) {
	Node<T> *run = list->head;
	while (run != NULL) {
		string str;
		if (getline(fp, str)) {
			LinkedList<T> *tmpList = new LinkedList<int>(str);
			run->setChild(tmpList->head);
			readNode(fp, tmpList);
			delete tmpList;
			istringstream iss(str);
		} else {
			break;
		}
	}
}

void preOrder(Tree &tree) {
	if (tree == NULL) {
		return;
	}
	if (tree->root == NULL) {
		return;
	}
	Node<int> *node = tree->root;
	cout << node->data << endl;
	if (node->firstChild == NULL) {
		return;
	} else {
		LinkedList<int> *list = new LinkedList<int>(node->firstChild);
		Node<int> *run = list->head;
		while (run != NULL) {
			Tree tmpTree = new TreeADT<int>();
			tmpTree->root = run;
			preOrder(tmpTree);
			delete tmpTree;
			run = run->nextSibling;
		}
	}
}

void postOrder(Tree &tree) {
	if (tree == NULL) {
		return;
	}
	if (tree->root == NULL) {
		return;
	}
	Node<int> *node = tree->root;
	if (node->firstChild == NULL) {
		return;
	} else {
		LinkedList<int> *list = new LinkedList<int>(node->firstChild);
		Node<int> *run = list->head;
		while (run != NULL) {
			Tree tmpTree = new TreeADT<int>();
			tmpTree->root = run;
			postOrder(tmpTree);
			delete tmpTree;
			run = run->nextSibling;
		}
	}
	cout << node->data << endl;
}

Tree createTree(const char *filename) {
	fstream fp;
	fp.open(filename, ios::in);

	Tree tree = new TreeADT<int>();

	string str;
	int parseData;

	LinkedList<int> *tmpList = new LinkedList<int>();

	getline(fp, str);

	istringstream iss(str);

	if (iss >> parseData) {
		tree->root = new Node<int>(parseData);
		tmpList->head = tree->root;
		tmpList->size++;
	}

	if (tmpList->size > 0) {
		readNode(fp, tmpList);
	}
	fp.close();
	return tree;
}

int main() {
	Tree tree = createTree("Bai1.txt");
	preOrder(tree);
	postOrder(tree);
	return 0;
}