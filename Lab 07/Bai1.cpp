#include <iostream>
#include <fstream>

using namespace std;

struct Node {
	int data;
	Node *left;
	Node *right;

	Node(int data) {
		this->data = data;
		this->left = NULL;
		this->right = NULL;
	}

	bool isLeaf() { // Neu node khong co node con
		return (left == NULL && right == NULL);
	}
};

class BSTree {


public:

	Node* root = NULL;
/**
 * Class constructors and destructor
 */

	BSTree() {

	}

	BSTree(Node* root) {
		this->root = root;
	}

	BSTree(const char* filename) {
		fstream fp;
		int data;
		fp.open(filename, ios::in);
		while (fp >> data) {
			insert(data, root);
		}
		fp.close();
	}

	~BSTree() {
		if (root != NULL) {
			removeNode(root);
		}
	}

/**
 * Extended methods
 */

	Node* searchParent(int value, Node* &start) {
		// key: key which is used to start searching
		if (start == NULL) {
			return NULL;
		}
		if (start->data == value) {
			return NULL;
		}
		if (start->left->data == value || start->right->data == value) {
			return start;
		}
		if (start->data > value) {
			return searchParent(value, start->left);
		} else {
			return searchParent(value, start->right);
		}
	}

	void removeNode(Node* &node) {
		if (! node->isLeaf()) {
			if (node->left != NULL) {
				removeNode(node->left);
			}
			if (node->right != NULL) {
				removeNode(node->right);
			}
		}
		delete node;
	}

	void insert(int data, Node* &key) {
		if (key == NULL) {
			key = new Node(data);
			return;
		}
		if (key->data == data) {
			return;
		}
		if (key->data > data) {
			insert(data, key->left);
			return;
		}
		if (key->data < data) {
			insert(data, key->right);
			return;
		}
	}

	void printInternal(Node* &node, bool isRoot = true) {
		if (node == NULL) {
			return;
		}
		if (isRoot) {
			cout << node->data << endl;
		}
		if (node->left != NULL) {
			cout << node->left->data << endl;
		}
		if (node->right != NULL) {
			cout << node->right->data << endl;
		}
		if (node->left != NULL) {
			printInternal(node->left, false);
		}
		if (node->right != NULL) {
			printInternal(node->right, false);
		}

	}

	Node* search(int data, Node* &node) {
		if(node == NULL) {
			return NULL;
		}
		if (node->data == data) {
			return node;
		}
		if (node->isLeaf()) {
			return NULL;
		}
		if (node->data > data) {
			return search(data, node->left);
		}
		if (node->data < data) {
			return search(data, node->right);
		}
	}

/**
 * Required methods
 */

	Node* searchValue(int value) {
		return search(value, root);
	}

	void insertValue(int value) {
		insert(value, root);
	}

	void printTree() {
		printInternal(root);
	}

	void deleteValue(int value) {
		Node* node = searchValue(value);
		if (node == NULL) { // value khong co trong tree
			return;
		}
		Node* parent = searchParent(value, root);
		if (parent == NULL) { // value chinh la root
			Node* run = node->right;
			root = run;
			while (run->left != NULL) {
				run = run->left;
			}
			run->left = node->left;
			delete node;
			return;
		}
		Node* &parentChild = (parent->left == node) ? parent->left : parent->right;
		if (node->isLeaf()) {
			parentChild = NULL;
			return;
		}
		if (node->left == NULL) {
			parentChild = node->right;
			delete node;
			return;
		}
		if (node->right == NULL) {
			parentChild = node->left;
			delete node;
			return;
		}
		parentChild = node->right;
		int data = node->left->data;
		delete node->left;
		delete node;
		insert(data, parent);
	}
};

int main() {
	BSTree* tree = new BSTree("Bai1.txt");
	// tree->printTree();
	tree->deleteValue(50);
	tree->printTree();
}
